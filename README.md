# tinymce

A powerful JavaScript-based rich-text-editor, which is used as the WYSIWYG-editor in the TYPO3-backend. The extension
offers a simple API to ease the embedding of the library.

# How to update the sources?

1. Update tinymce with npm
2. Download the latest language pack from "http://archive.tinymce.com/i18n/index.php".
   You can simplify the selection by using the following command in your developer tools console.
   Deselect "ru@petr1708" and "zh_CN.GB2312" afterwards, because this language doesn't exists and leads to errors.
    
	document.querySelectorAll('input[type=checkbox]').forEach(function(element) { element.checked = true; });

3. Rename node_modules to tinymce_mode_modules, because TYPO3 removes the node_modules directory
4. Add the "langs" directory inside tinymce_node_modules/tinymce with the downloaded languages
