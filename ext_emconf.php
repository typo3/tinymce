<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'tinyMCE',
	'description' => 'TinyMCE sources including a small PHP API',
	'category' => 'misc',
	'version' => '5.0.4',
	'state' => 'stable',
	'uploadfolder' => FALSE,
	'createDirs' => '',
	'clearcacheonload' => FALSE,
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan@sgalinski.de',
	'author_company' => 'sgalinski Internet Services',
	'autoload' =>
		[
			'psr-4' => ['SGalinski\\Tinymce\\' => 'Classes']
		],
	'constraints' =>
		[
			'depends' =>
				[
					'php' => '7.0.0-7.2.99',
					'typo3' => '7.6.0-8.7.99',
				],
			'conflicts' => [],
			'suggests' => [],
		],
];